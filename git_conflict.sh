#!/bin/bash
#https://asciinema.org/a/cyzy6mbwsbbr8riulve39rr3d
#créer un dossier git-repo et y entrer
mkdir git-repo
cd git-repo
# initialiser git
git init
# créer le fichier et l'ajouter à l'index
touch my_code.sh
git add my_code.sh
# mettre du texte dans le fichier
echo "echo Hello" > my_code.sh
# commit
git commit -am 'initial'

# creer une nouvelle branche
git checkout -b new_branch
# changer le texte du fichier 
echo "echo \"Hello World\"" > my_code.sh
# commit dans la nouvelle branche
git commit -am 'first commit on new_branch'
#revenir au master
git checkout master
# rechanger le fichier +!
echo 'echo "Hello World!"' > my_code.sh
#commit
git commit -am 'second commit on master'
git merge new_branch

